# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
s1 = Student.create(first_name: 'Alfred', last_name: 'Aalborg', age: 18, location: 'Jakarta')
s1 = Student.create(first_name: 'Brandon', last_name: 'Babcock', age: 19, location: 'Little Rock')
s1 = Student.create(first_name: 'Christian', last_name: 'Cantu', age: 20, location: 'Guadalajara')
s1 = Student.create(first_name: 'Domingo', last_name: 'Duarte', age: 21, location: 'Nice')
s1 = Student.create(first_name: 'Elise', last_name: 'Eggleton', age: 22, location: 'London')
s1 = Student.create(first_name: 'Franklin', last_name: 'Fabourg', age: 23, location: 'Manila')
s1 = Student.create(first_name: 'Grace', last_name: 'Gobbu', age: 24, location: 'Valparaiso')