Rails.application.routes.draw do
  resources :students, only: [:index, :show, :update, :create]
end
